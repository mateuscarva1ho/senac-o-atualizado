﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Senacao.Service
{
    class HttpService
    {

        static readonly HttpClient Client = new HttpClient();


        public static async Task<HttpResponseMessage>
            GetRequest(string url)
        {
            HttpResponseMessage response = await Client.GetAsync(url);
            return response;
        }

        public static async Task<HttpResponseMessage>
            PostResquest(string url, object obj)
        {
            //serializar objeto que será enviado para parametro
            string json = JsonConvert.SerializeObject(obj);

            //preparando dados para envio
            StringContent strContent = new StringContent(json, Encoding.UTF8, "application/json");

            //realizando chamada http
            HttpResponseMessage response = await Client.PostAsync(url, strContent);

            return response;
        }

        public static JObject
            GetErrorDataFromHttpResponseMessage(HttpResponseMessage response)
        {
            //convertendo json para um objeto c#
            var data = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            return data;
        }
    }
}



